import React, { useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import { Canvas, useFrame } from 'react-three-fiber';
import { PCFSoftShadowMap, ObjectLoader } from 'three';
import './index.css';
//import App from './App';
import * as serviceWorker from './serviceWorker';

let models

function Box(props) {
  // This reference will give us direct access to the mesh
  const mesh = useRef()
  
  // Set up state for the hovered and active state
  const [hovered, setHover] = useState(false)
  const [active, setActive] = useState(false)
  
  // Rotate mesh every frame, this is outside of React without overhead
  useFrame(() => (mesh.current.rotation.x = mesh.current.rotation.y += 0.01))
  
  return (
    <mesh
      {...props}
      ref={mesh}
      scale={active ? [1.5, 1.5, 1.5] : [1, 1, 1]}
      onClick={e => setActive(!active)}
      onPointerOver={e => setHover(true)}
      onPointerOut={e => setHover(false)}>
      <boxBufferGeometry attach="geometry" args={[1, 1, 1]} />
      <meshStandardMaterial attach="material" color={hovered ? 'hotpink' : 'orange'} />
    </mesh>
  );
}

function Ground(props) {
  const mesh = useRef();

  return (
    <mesh
      {...props}
      ref={mesh}>
      <planeGeometry attach="geometry" width={1000} height={1000} rotateX={- Math.PI / 2} />
      <meshStandardMaterial attach="material" color="#fff" metalness={0} emissive="#000000" roughness={0} />
    </mesh>
  )
}

ReactDOM.render(
  <Canvas
    gl={{ shadowMap: { enabled: true, type: PCFSoftShadowMap } }}
    camera={{
      width: window.innerWidth,
      height: window.innerHeight,
      isPerspectiveCamera: true,
      position: [3, 16, 111],
      fov: 20,
      near: 1,
      aspect: 1000,
      far: window.innerWidth / window.innerHeight}}
  >
    <ambientLight color="#fff" />
    <pointLight position={[10, 10, 10]} />
    <Ground position={[0, 0, 0]} />
  </Canvas>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
