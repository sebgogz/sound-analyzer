var cookieName = 'cookie_city3D';

function writeCookie(name, value) {
	if (navigator.cookieEnabled) {	// On vÃ©rifie si l'utilisateur a bien activÃ© les cookies
		var date = new Date();
		var expires = new Date();
		expires.setTime(date.getTime() + (365*24*60*60*1000));
		document.cookie = name + "=" + encodeURIComponent(value) + ";expires=" + expires.toGMTString();
	}
}

function readCookie(name) {
	if (navigator.cookieEnabled) {
		var cookContent = document.cookie, cookEnd, i, j;
		var name = name + '=';

		for (i = 0, c = cookContent.length ; i < c ; i++)	// On cherche notre cookie parmis les cookies (il peut y en avoir plusieurs, un ou aucun)
		{
			j = i + name.length;
			if (cookContent.substring(i, j) == name)
			{
				cookEnd = cookContent.indexOf(';', j);
				if (cookEnd == -1)
					cookEnd = cookContent.length;
				return decodeURIComponent(cookContent.substring(j, cookEnd));
			}
		}
	}
	return null;
}

function checkCookie() {
	var myCookie = readCookie(cookieName);
		
	if (myCookie == null) {	// Si notre cookie n'existe pas, on le crÃ©e
		if (webGL_test() && !mobile_test()) {
			writeCookie(cookieName, 'enabled');
			enable3D();
		}
		else
			writeCookie(cookieName, 'disabled');
	}
	else if (myCookie == 'enabled') {
		enable3D();
	}
}
